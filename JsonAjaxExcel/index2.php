<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="table-respoonsive">
			<h2 align="center">Fetch JSON Data to HTML Table with the help of jQuery</a></h2><br/>
<table class="table table-bordered table-striped" id="emp_table">
    <tr>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Job Title</td>
        <td>Emp Code</td>
        <td>Phone Number</td>
        <td>Email</td>
    </tr>
</table>
            </div>          
</div>
</body>
</html>
<script>
    $(document).ready(function(){
      $.getJSON("emp.json",function(data){
        var emp_data = '';
        $.each(data.Employees,function(key,value){
    emp_data += '<tr>';
    emp_data += '<td>'+value.firstName+'</td>';
    emp_data += '<td>'+value.lastName+'</td>';
    emp_data += '<td>'+value.jobTitleName+'</td>';
    emp_data += '<td>'+value.employeeCode+'</td>';
    emp_data += '<td>'+value.phoneNumber+'</td>';
    emp_data += '<td>'+value.emailAddress+'</td>';
    emp_data += '</tr>';
        });
        $('#emp_table').append(emp_data);
      });
    });
</script>