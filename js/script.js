var username="";
        function send_message(message){
            var prevMssg=$("#container").html();
        //prevMssg.length gives the width of the empty div tag which is 3
            if(prevMssg.length>3){
                prevMssg=prevMssg+"<br>";
            }
            $("#container").html(prevMssg+"<span class='current_message'>"+"<span class='bot'>Chatbot: </span>"+message+"</span>");
            $($(".current_message")).hide();   //it will display mssg from the bot and than will hide it and than after the delay of 500ms 
            $($(".current_message")).delay(1000).fadeIn();//it will display the mssg.
            $($(".current_message")).removeClass("current_message");

           
        }
        function get_username(){
          send_message("Hello, what is your name") ;
        }
        function ai(message){
            if(message.indexOf('how are you')>=0){
                send_message("Thanks, I am good!");
             }
             if(message.indexOf('who are you')>=0){
                send_message("I am a chatting bot created by developers.");
             }
         if(username.length<3){
            username=message;
            send_message("Nice to meet you "+username+", how are you doing?") ;
         }
         if((message.indexOf("time")>=0)||(message.indexOf("hours")>=0)){
            var date=new Date();
            var h =date.getHours();
            var m=date.getMinutes();
            send_message("Current time is:"+h+":"+m);
         }
         if((message.indexOf("nice")>=0)||(message.indexOf("good")>=0)||
         (message.indexOf("awesome")>=0)||(message.indexOf("well")>=0)||(message.indexOf("excellent")>=0)){
            send_message("Great!<br/>how can i help you");
         }
         
        }
    $(function(){
        get_username();
        $("#textbox").keypress(function(event){
       if( event.which==13){  //13 stands for enter
        if($("#enter").prop("checked")){
            $("#send").click();
        event.preventDefault();
        }
       }
        });
        $("#send").click(function(){
            var username="<span class='username'>You: </span>";
            var mssg =$("#textbox").val();
        $("#textbox").val("");
        var prevMssg=$("#container").html();
        //prevMssg.length gives the width of the empty div tag which is 3
        if(prevMssg.length>3){
            prevMssg=prevMssg+"<br>";
        }
        $("#container").html(prevMssg+username+mssg);
        $("#container").scrollTop($("#container").prop("scrollHeight"));
        ai(mssg);
        });
    });