$("#formValidation").validate({
    rules:{
        name:{
            required : true,
            minlength:2
        },
        email:{
            required : true,
            email:true
        },
        phone:{
            required : true,
            number:true,
            minlength:10,
            maxlength:10
        },
        pwd:{ required :true,
        minlength:5 
              },
        cpwd:{
            required :true,
            equalTo:'#pwd'
        }
    },
    messages:{
        name:{
        required:"Please enter your name",
        minlength:"Name atleast 2 characters"
        },
        email:"Please enter your email",
        phone:"Please enter your phone",
        subject:"Please enter subject",
        formMessage:"Please enter Message"
    },
    submitHandler: function(form) {
        alert("Submitted");
      form.submit();
    }
   });