<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='../css/main.css'>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/cupertino/jquery-ui.css">
    <script src='main.js'></script>
      <script src="https://code.jquery.com/jquery-3.7.0.min.js" 
        integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <style>
      
      </style>  
</head>
<body>
<div class="input_container" >
    <form name="formValiadtion" id="formValidation" action="form_submit.php" method="post">
        <div class="input-row">
            <input type="text" name="name" placeholder="Name*" required>
        </div>
        <div class="input-row">
            <input type="email" name="email" placeholder="Email*" required>
        </div>
        <div class="input-row">
            <input type="text" name="phone" placeholder="Phone*" required>
        </div>
        <div class="input-row">
        <input type="password" id="pwd" name="pwd" placeholder="Password" required>
        </div>
        <div class="input-row">
        <input type="password" id="Cpwd" name="cpwd" placeholder="Password Again" required>
        </div>
        <div class="input-row">
            <input type="text" name="subject" placeholder="Subject*" required>
        </div>
        <div class="input-row">
          <textarea placeholder="Message*" name="formMessage" required></textarea>
        </div>
        <div class="text-center">
            <input type="submit" value="Submit">
        </div>
    </form>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="../js/jquery.validate.min.js"></script>
<script src="../js/custom.js"></script>

</body>
</html>