<?php
include "connection.php";
function fill_unit_select_box($con){
    $output='';
    $query="SELECT * FROM unit order by unit_name asc";
    $res =$con->query($query);
    foreach($res as $row){
        $output.='<option value="'.$row["unit_name"].'">' .$row["unit_name"]. '</option>';
    }
    return $output;
}

?>
<!doctype html>
<html>
    <head>
        <title>Dyanmic rows</title>
        <script src="https://code.jquery.com/jquery-3.7.0.min.js" 
        integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
        rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" 
        integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" 
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" 
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" 
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>
    <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" 
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" 
        crossorigin="anonymous"></script>
    <br/>
    <div class="container">
        <h3 align="center">Add or Remove Selectpicker Dropdown dynamically in php using Azax jQuery</h3>
    <div class="card">
        <div class="card-header">Enter Item Details</div>
        <div class="card-body">
            <form method="post" id="insert_form">
                <div class="table-responsive">
                    <span id="error"></span>
                    <table class="table table-bordered" id="item_table">
                        <tr>
                            <th>Enter Item Name</th>
                            <th>Enter Quantity</th>
                            <th>Select Unit</th>
                            <th><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus"></i>
                            </button></th>
                        </tr>
                    </table>
                    <div align="center">
                   <input type="submit" name="submit" id="submit_button" class="btn btn-primary" value="Insert" />
                </div>
            </form>
        </div>
    </div>
    </div>
    </body>
</html>
<script>
    $(document).ready(function(){
var count=0;
function add_input_field(count){
    var html='';
    html += '<tr>';

html += '<td><input type="text" name="item_name[]" class="form-control item_name" /></td>';

html += '<td><input type="text" name="item_quantity[]" class="form-control item_quantity" /></td>';

html += '<td><select name="item_unit[]" class="form-control selectpicker" data-live-search="true"><option value="">Select Unit</option><?php echo fill_unit_select_box($con); ?></select></td>';

var remove_button = '';

if(count > 0)
{
    remove_button = '<button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-minus"></i></button>';
}

html += '<td>'+remove_button+'</td></tr>';

return html;
}
$('#item_table').append(add_input_field(0)); //to show a blank row tab for the 1st row when no insertion is made.And for 1st
$('.selectpicker').selectpicker('refresh'); //row there will not be any remove button
$(document).on('click', '.add', function(){

count++;   //add more rows by clicking on add button

$('#item_table').append(add_input_field(count));

$('.selectpicker').selectpicker('refresh');


});

$(document).on('click', '.remove', function(){
                                       //remove added rows by clicking on minus icon
$(this).closest('tr').remove();

});

$('#insert_form').on('submit', function(event){

event.preventDefault(); //e.preventDefault() to submit form data without reloading the page.

var error = '';

count = 1;

$('.item_name').each(function(){

    if($(this).val() == '')
    {

        error += "<li>Enter Item Name at "+count+" Row</li>";

    }

    count = count + 1;

});

count = 1;

$('.item_quantity').each(function(){

    if($(this).val() == '')
    {

        error += "<li>Enter Item Quantity at "+count+" Row</li>";

    }

    count = count + 1;

});

count = 1;

$("select[name='item_unit[]']").each(function(){

    if($(this).val() == '')
    {

        error += "<li>Select Unit at "+count+" Row</li>";

    }

    count = count + 1;

});

var form_data = $(this).serialize(); //serialise method will convert form data into encoded url string

if(error == '')
{  //write ajax request to submit form data to server

    $.ajax({

        url:"insert.php",

        method:"POST",

        data:form_data,

        // beforeSend:function()
        // {

        //     $('#submit_button').attr('disabled', 'disabled');

        // },

        success:function(data)
        {

            // if(data == 'ok')
            // {    
                //to reset form field table all rows data

                $('#item_table').find('tr:gt(0)').remove();

                $('#error').html('<div class="alert alert-success">Item Details Saved</div>');

                $('#item_table').append(add_input_field(0)); //to append 1 blank row after all the table data is reset

                $('.selectpicker').selectpicker('refresh');

                // $('#submit_button').attr('disabled', false);
            // }

        }
    })

}
else
{
    $('#error').html('<div class="alert alert-danger"><ul>'+error+'</ul></div>');
}

});

});

</script>