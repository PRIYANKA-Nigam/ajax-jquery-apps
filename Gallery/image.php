<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/img.css">
  

    <title>JQuery image Portfolio</title>
</head>
<body>
    <div id="header">
        <div id="menu">
            <h2>jQuery Portfolio</h2>
            <input type="text" placeholder="search..." style="float:right" id="search" />
        </div>
    </div>
    <div id="overlay"></div>
    <div id="frame">
        <table id="frame-table">
            <tr>
                <td id="left">
              <img src="../images/le.png" alt="Left" width="80px" height="80px" />
                </td>
                <td id="right">
               <img src="../images/ri.png" alt="Right" width="80px" height="80px"/>
                </td>
            </tr>
        </table>
        <img id="main" src="" alt=""/>
        <div id="desc">
        <p></p>
          </div>  

    </div>
    <div id="wrapper">
        <ul id="filter">
            <h1><div id="aa" style="background:red;width:80%;height:50px;display:block;"><center><b><i>
                <u>Image Portfolio</u></i></b></center></div></h1>
            <li class="active">all</li>
            <li>nature</li>
            <li>architecture</li>
            <li>city</li>
            <li>ecosystem</li>
        </ul>
        <ul id="portfolio">
            <?php include_once("list.html")  ?>
        </ul>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" 
    crossorigin="anonymous"></script>
   
    <script>
        $(function(){
            var current_li;
            $("#search").keyup(function(){
            var current_query = $("#search").val();
            if(current_query!=""){
                $("#portfolio li").hide();  
            $("#portfolio li").each(function(){
             var current_keyword = $(this).attr("data-keywords");
             if(current_keyword.indexOf(current_query)>=0){
                $(this).show();
             } //with keypress() the searchable item is shown but on removing that item from search bar 
            });//it doesn't show back all the items.so,replacing keypress() with keyup()
            }else{
                $("#portfolio li").show();
            }
         
            });
            function setImg(src,id){
               // $("#frame img").attr("src",src); or
              $("#main").attr("src",src);
              var path="text/"+id+".txt";
              $.get(path,function(data){
              // console.log(data);
               $("#desc p").html(data);
              });
            }
$("#portfolio img").click(function(){
var src=$(this).attr("src");
var id= $(this).attr("id");
current_li=$(this).parent();
setImg(src,id);
$("#frame").fadeIn();
$("#overlay").fadeIn();
});
// jquery reduces a lot of code lines and if we right the same code using js it will occupy lots many line of codes
$("#overlay").click(function(){
 $(this).fadeOut();
 $("#frame").fadeOut();
    });
    $("#right").click(function(){
        if(current_li.is(":last-child")){
        var next_li= $("#portfolio li").first();
        }else{
            var next_li=current_li.next();
        }
      var next_li=current_li.next();
      var next_src= next_li.children("img").attr("src");
      var id= next_li.children("img").attr("id");
    //   $("#main").attr("src",next_src);
      setImg(next_src,id);
      current_li=next_li;
    });
    $("#left").click(function(){
        if(current_li.is(":first-child")){
        var prev_li= $("#portfolio li").last();
        }else{
            var prev_li=current_li.prev();
        }
     var prev_li = current_li.prev();
     var prev_src =prev_li.children("img").attr("src");
     var id =prev_li.children("img").attr("id");
    //  $("#main").attr("src",prev_src);
     setImg(prev_src,id);
     current_li=prev_li;
});
$("#right,#left").mouseover(function(){
$(this).css("opacity","0.75");
});
$("#right,#left").mouseleave(function(){
$(this).css("opacity","0.55");
});
});
///////////////////////////another jquery /////////////////////////////
$(function(){
$("#filter li").click(function(){
var category = $(this).html();
$("#aa").html(category);//The html() method sets or returns the content (innerHTML) of the selected elements.
$("#filter li").removeClass("active");
$(this).addClass("active");
// $("#portfolio li").fadeOut(); //on clicking on any category all images will disappear
//or
$("#portfolio li").hide();
$("#portfolio li").each(function(){
    if($(this).hasClass(category)){
        // $(this).fadeIn(); or
        $(this).show();
    }
// var test = $(this).html();
});

});
});
    </script>
</body>
</html>